-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.6-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para universidad
CREATE DATABASE IF NOT EXISTS `universidad` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `universidad`;

-- Volcando estructura para tabla universidad.asignaturas
CREATE TABLE IF NOT EXISTS `asignaturas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) DEFAULT NULL,
  `observacion` varchar(50) DEFAULT NULL,
  `creditos` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla universidad.asignaturas: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `asignaturas` DISABLE KEYS */;
INSERT INTO `asignaturas` (`id`, `descripcion`, `observacion`, `creditos`) VALUES
	(10, 'Programación Orientada a Objetos ', 'Programación Orientada a Objetos ', 3),
	(11, 'Calculo I', 'Calculo I', 4),
	(12, 'Electromecánica', 'Electromecánica', 3),
	(13, 'Diseño de Base de Datos', 'Diseño de Base de Datos', 5),
	(14, 'Humanidades I', 'Humanidades I', 2),
	(16, 'Diseño de Paginas Web', 'Diseño de Paginas Web', 2);
/*!40000 ALTER TABLE `asignaturas` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) DEFAULT NULL,
  `observacion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla universidad.roles: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `descripcion`, `observacion`) VALUES
	(1, 'Secretaria', 'Secretaria'),
	(2, 'Profesor(a)', 'Profesor(a)'),
	(3, 'Alumno(a)', 'Alumno(a)');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` varchar(50) DEFAULT NULL,
  `nombres` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `celular` varchar(10) DEFAULT NULL,
  `rol_id` int(11) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rol_id` (`rol_id`),
  CONSTRAINT `FK_usuarios_roles` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla universidad.usuarios: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `cedula`, `nombres`, `apellidos`, `email`, `celular`, `rol_id`, `password`) VALUES
	(20, '1144164462', 'Richard Alejandro', 'Caicedo Rosero', 'alejo7532@hotmail.com', '3108325040', 3, 'e10adc3949ba59abbe56e057f20f883e'),
	(21, '123456789', 'Maylin ', 'Muñoz', 'maylin@gmail.com', '3108325040', 1, 'e10adc3949ba59abbe56e057f20f883e'),
	(22, '987654321', 'Juan Pablo', 'Rodriguez Gomez', 'juan.pablo@gmail.com', '7894562536', 2, 'e10adc3949ba59abbe56e057f20f883e');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.usuario_asignatura
CREATE TABLE IF NOT EXISTS `usuario_asignatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `id_asignatura` int(11) DEFAULT NULL,
  `creditos` int(11) DEFAULT NULL,
  `calificacion` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_asignatura` (`id_asignatura`),
  CONSTRAINT `FK_usuario_asignatura_asignaturas` FOREIGN KEY (`id_asignatura`) REFERENCES `asignaturas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_usuario_asignatura_usuarios` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla universidad.usuario_asignatura: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario_asignatura` DISABLE KEYS */;
INSERT INTO `usuario_asignatura` (`id`, `id_usuario`, `id_asignatura`, `creditos`, `calificacion`) VALUES
	(39, 20, 14, 2, 4.5),
	(40, 20, 13, 5, 0),
	(41, 20, 10, 3, 0);
/*!40000 ALTER TABLE `usuario_asignatura` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
