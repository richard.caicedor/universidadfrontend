import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  titulo = 'Gestión Matriculas';
  public inicio_sesion:string;
  public nombre_user:string;
  public perfil:string;

  constructor(
  	private _router:Router
  ){
  	this.inicio_sesion = localStorage.getItem('inicio_storage');
    this.nombre_user = localStorage.getItem('nombre');
    this.perfil = localStorage.getItem('perfil');
  }

  ngOnInit(){
  	if(this.inicio_sesion == null ){ 
  		this._router.navigate(['/login']); 
  	}
  }

  cerrarSesion(){
    localStorage.clear();
    location.reload(); 
  }
}
