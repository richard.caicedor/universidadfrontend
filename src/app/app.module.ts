import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { PdfViewerModule } from 'ng2-pdf-viewer';

// Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { ListUsuariosComponent } from './usuario/list-usuarios.component';
import { AsignaturasComponent } from './asignaturas/asignaturas.component';
import { ListAsignaturasComponent } from './asignaturas/list-asignaturas.component';
import { MatriculaComponent } from './matricula/matricula.component';
import { ListMatriculaComponent } from './matricula/list-matricula.component';
import { ProfesorComponent } from './profesor/profesor.component';

// Rutas
import { routing, appRoutingProviders } from './app.routing';
import { AuthGuard } from './guards/auth.guard';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsuarioComponent,
    ListUsuariosComponent,
    AsignaturasComponent,
    ListAsignaturasComponent,
    MatriculaComponent,
    ListMatriculaComponent,
    ProfesorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    PdfViewerModule
  ],
  providers: [ 
    AuthGuard,
  	appRoutingProviders
  ],
  bootstrap: [AppComponent]
})  
export class AppModule { }
