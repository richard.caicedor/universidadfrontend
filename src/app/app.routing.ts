import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Componentes
import { LoginComponent } from './login/login.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { ListUsuariosComponent } from './usuario/list-usuarios.component';
import { AsignaturasComponent } from './asignaturas/asignaturas.component';
import { ListAsignaturasComponent } from './asignaturas/list-asignaturas.component';
import { AuthGuard } from './guards/auth.guard';
import { AppComponent } from './app.component';
import { MatriculaComponent } from './matricula/matricula.component';
import { ListMatriculaComponent } from './matricula/list-matricula.component';
import { ProfesorComponent } from './profesor/profesor.component';


const appRoutes: Routes = [
	{path: 'login', component: LoginComponent },
	{path: 'usuario', component: UsuarioComponent, canActivate: [AuthGuard]},
	{path: 'list-users', component: ListUsuariosComponent , canActivate: [AuthGuard]},
	{path: 'asignatura', component: AsignaturasComponent, canActivate: [AuthGuard]},
	{path: 'list-asignaturas', component: ListAsignaturasComponent, canActivate: [AuthGuard]},
	{path: 'matricula', component: MatriculaComponent, canActivate: [AuthGuard]},
	{path: 'list-matricula', component: ListMatriculaComponent, canActivate: [AuthGuard]},
	{path: 'estudiantes', component: ProfesorComponent, canActivate: [AuthGuard]},
];

export const appRoutingProviders:any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
  