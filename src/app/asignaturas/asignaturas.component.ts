import { Component, OnInit } from '@angular/core';
import { Asignatura } from '../models/asignatura';
import { AsignaturaService } from '../services/asignatura.service';

@Component({
  selector: 'app-asignaturas',
  templateUrl: './asignaturas.component.html',
  providers: [AsignaturaService]
})
export class AsignaturasComponent implements OnInit {

	public asignatura:Asignatura;
  public _mensaje:string;
  public styleMensaje:string;

  constructor(public _asignaturaService:AsignaturaService){ 
  	this.asignatura = new Asignatura(0,'','',0);
  }

  ngOnInit() {
  } 

  onGuardar(){
  	this._asignaturaService.addAsignaturas(this.asignatura).subscribe(
      // Funciones de calban 
      response => {
        console.log(response)
        this._mensaje = response.status;
        this.styleMensaje = 'alert-success';
        this.asignatura = new Asignatura(0,'','',0);
      }, 
      error => {
        console.log(<any>error);
      }
    );
  }


}
