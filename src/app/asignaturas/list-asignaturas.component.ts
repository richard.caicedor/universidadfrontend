import { Component, OnInit } from '@angular/core';
import { Asignatura } from '../models/asignatura';
import { AsignaturaService } from '../services/asignatura.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-listAsignaturas',
  templateUrl: './list-asignaturas.html', 
  providers: [AsignaturaService]
})
export class ListAsignaturasComponent implements OnInit {

	public _asignaturas:Asignatura[];


	constructor(
    public _asignaturaService:AsignaturaService,
    private _router:Router
  ) {}

	ngOnInit() {
    this.getAsignaturas();
  }
   
  getAsignaturas(){
    this._asignaturaService.getAsignaturas().subscribe(
      result => {
        this._asignaturas = result;
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  OnEliminar(id:number){
    this._asignaturaService.deleteAsignatura(id).subscribe(
      result =>{ 
        alert('Registro Eliminado');
        this.getAsignaturas();  
      }, error =>{
        console.log(error);
      }
      );
  }
 
}
 