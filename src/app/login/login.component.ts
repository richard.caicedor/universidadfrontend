import { Component, OnInit } from '@angular/core';
import { Login } from '../models/login'
import { UsuarioService } from '../services/usuario.service'; 
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [UsuarioService]
})
export class LoginComponent implements OnInit {

	public _login:Login;
	public _mensaje:string;


  constructor( private _usuarioService:UsuarioService, private _router:Router) { 
  	this._login = new Login('','');
  }

  ngOnInit() {
  	if(localStorage.getItem('inicio_storage') != null ){ 
  		this._router.navigate(['/']); 
  	} 
  }
 
  onValidar(){
  	this._usuarioService.getUsuario(this._login).subscribe(
  		result => { 
  			if(result.status == 200){
  				localStorage.setItem('inicio_storage', 'true');
  				localStorage.setItem('nombre', result.data.nombres + ' '+ result.data.apellidos);
  				localStorage.setItem('perfil', result.data.rol_id);
          localStorage.setItem('id_user', result.data.id);
  				location.reload(); 
  			}else{
  				this._mensaje = 'Error! Las credenciales son Incorrectas';
  			}
  		}, error => {
  			console.log(error);
  		} 
  	);
  }
  
}
