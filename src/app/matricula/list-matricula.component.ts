import { Component, OnInit } from '@angular/core';
import { MatriculaService } from '../services/matricula.service';
import { Asignatura } from '../models/asignatura';

@Component({
  selector: 'app-listmatricula',
  templateUrl: './list-matricula.html',
  providers: [MatriculaService]
})
export class ListMatriculaComponent implements OnInit {
 
	public _asignaturas:Array<string> = [];

  constructor(
  	public _matriculaService:MatriculaService
   ) { 
  }

  ngOnInit() {
  	this.getAsignaturas();
  }

  getAsignaturas(){
    this._matriculaService.getCalificaciones().subscribe(
      result => {
        this._asignaturas = result;
      },
      error => {
        console.log(<any>error);
      }
    );
  }

}
