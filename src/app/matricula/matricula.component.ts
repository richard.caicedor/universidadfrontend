import { Component, OnInit } from '@angular/core';
import { MatriculaService } from '../services/matricula.service';
import { Asignatura } from '../models/asignatura';

@Component({
  selector: 'app-matricula',
  templateUrl: './matricula.component.html',
  providers: [MatriculaService]
})
export class MatriculaComponent implements OnInit {

	public _asignaturas:Array<string> = [];
	public _arrayAsig: Array<string> = [];
	public contTotal:number;
	public mensaje:string;
	public styleMensaje:string;
	public contReal:number;

  constructor(
  	public _matriculaService:MatriculaService
   ) { 
   	this.contTotal =0;
   	this.mensaje ='Credítos: ';
  	this.styleMensaje ='badge badge-success'
  }

  ngOnInit() {
  	this.getAsignaturas();
  }

  onSeleccionar(event){
  	var arrayItem2=[];
  	arrayItem2 = event.target.value.split('-');
  	if(event.target.checked && (this.contTotal + parseInt(arrayItem2[1])) <= 10){
  		this._arrayAsig.push(event.target.value);
  		this.mensaje ='Credítos: ';
  		this.styleMensaje ='badge badge-success';
  	}else{
  		this._arrayAsig.forEach( (item, index) => {
		    if(item === event.target.value) this._arrayAsig.splice(index,1);
		});
	  	this.mensaje ='Máximo hasta 10 Credítos: ';
  		this.styleMensaje ='badge badge-danger';
  		event.target.checked=false;
  	} 
  	var arrayItem=[];
  	var cont = 0;
  	this._arrayAsig.forEach( (item, index) => {
		arrayItem = item.split('-');
		cont += parseInt(arrayItem[1]);
	});
	
	this.contTotal =cont+this.contReal;
  }

  getAsignaturas(){
  	let cont =0;
    this._matriculaService.getAsignaturas().subscribe(
      result => {
        this._asignaturas = result;
        this._asignaturas.forEach( (item, index) => {
    		    cont+=item['creditosUA'];
    		});
        this.contReal = cont;
        this.contTotal = cont; 
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  onMatricular(){
  	this._matriculaService.onMatricular(this._arrayAsig).subscribe(
  		result => {
  			console.log(result)
  			this.getAsignaturas();
  		}, error => {
  			console.log(error);
  		}
  	);
  }

}
