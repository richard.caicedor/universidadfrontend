export class Asignatura{

	constructor(
		public id:number,
		public descripcion:string,
		public observacion:string,
		public creditos:number
	){}
}