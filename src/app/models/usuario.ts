export class Usuario {

	constructor(
		public id:number,
		public cedula:string,
		public nombres:string,
		public apellidos:string,
		public email:string,
		public celular:string,
		public rol_id:string,
		public password:string
	){}
}