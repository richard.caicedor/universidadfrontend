import { Component, OnInit } from '@angular/core';
import { ProfesorService } from '../services/profesor.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.component.html',
  providers: [ProfesorService]
})
export class ProfesorComponent implements OnInit {

	public _estudiantes:Array<string> = [];

  constructor(public _profesorService:ProfesorService) { }

  ngOnInit() {
  	this.getEstudiantes();
  }

  getEstudiantes(){
    this._profesorService.getEstudiantes().subscribe(
      result => {
        this._estudiantes = result;
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  onActualizar(event, id:string){
  	var array_info = [event.target.value,id]; 
  	this._profesorService.postActualizar(array_info).subscribe(
      result => {
        alert('Registro Actualizado')
      },
      error => {
        console.log(<any>error);
      }
    );
  }

}
