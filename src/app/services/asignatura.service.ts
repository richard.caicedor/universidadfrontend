import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { Asignatura } from '../models/asignatura'


@Injectable()

export class AsignaturaService{
	public url:string;

	constructor(
		public _http:Http
	){
		this.url = 'http://localhost:3000/';
	} 

	getAsignaturas(){
		return this._http.get(this.url+'asignatura').pipe(map(res => res.json()));
	}

	addAsignaturas(asignatura:Asignatura){
		let headers = new Headers({'Content-Type': 'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.post(this.url+'asignatura',asignatura,options).pipe(map(res => res.json()));
	}

	deleteAsignatura(id){
		let headers = new Headers({'Content-Type': 'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.delete(this.url+'asignatura/'+id).pipe(map(res => res.json()));
	}
} 

  