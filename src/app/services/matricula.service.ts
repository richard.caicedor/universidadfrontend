import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { Asignatura } from '../models/asignatura'


@Injectable()

export class MatriculaService{
	public url:string;

	constructor(
		public _http:Http
	){
		this.url = 'http://localhost:3000/';
	} 

	getAsignaturas(){
		var id = localStorage.getItem('id_user');
		return this._http.get(this.url+'asignatura/'+id).pipe(map(res => res.json()));
	}

	onMatricular(arrayAsig:Array<string>){
		let headers = new Headers({'Content-Type': 'application/json'});
		let options = new RequestOptions({headers: headers}); 
		return this._http.post(this.url+'matricula/'+localStorage.getItem('id_user'),arrayAsig,options).pipe(map(res => res.json()));
	}

	getCalificaciones(){
		var id = localStorage.getItem('id_user');
		return this._http.get(this.url+'calificacion/'+id).pipe(map(res => res.json()));
	}
	
} 

  