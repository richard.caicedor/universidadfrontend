import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { Asignatura } from '../models/asignatura'


@Injectable()

export class ProfesorService{
	public url:string;

	constructor(
		public _http:Http
	){
		this.url = 'http://localhost:3000/';
	} 

	getEstudiantes(){
		return this._http.get(this.url+'estudiantes').pipe(map(res => res.json()));
	}

	postActualizar(array_info){
		let headers = new Headers({'Content-Type': 'application/json'});
		let options = new RequestOptions({headers: headers}); 
		return this._http.put(this.url+'estudiantes',array_info,options).pipe(map(res => res.json()));

	}
} 

  