import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { Usuario } from '../models/usuario';
import { Login } from '../models/login';

@Injectable()

export class UsuarioService{
	public url:string;

	constructor(
		public _http:Http
	){
		this.url = 'http://localhost:3000/';
	}

	getUsuarios(){
		return this._http.get(this.url+'usuarios').pipe(map(res => res.json()));
	}

	addUsuario(usuario:Usuario){
		let headers = new Headers({'Content-Type': 'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.post(this.url+'usuarios',usuario,options).pipe(map(res => res.json()));
	}

	deleteUsuario(id){
		let headers = new Headers({'Content-Type': 'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.delete(this.url+'usuarios/'+id).pipe(map(res => res.json()));
	}

	getUsuario(login:Login){
		let headers = new Headers({'Content-Type': 'application/json'});
		let options = new RequestOptions({headers: headers});
		return this._http.post(this.url+'login',login,options).pipe(map(res => res.json()));
	}  
} 

 