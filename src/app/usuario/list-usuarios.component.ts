import { Component, OnInit } from '@angular/core';
import { Usuario } from '../models/usuario';
import { UsuarioService } from '../services/usuario.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-listUsuarios',
  templateUrl: './list-usuarios.html',
  providers: [UsuarioService]
})
export class ListUsuariosComponent implements OnInit {

	public _usuarios:Usuario[];


	constructor(
    public _usuarioService:UsuarioService,
    private _router:Router
  ) {}

	ngOnInit() {
    this.getUsuarios();
  }
   
  getUsuarios(){
    this._usuarioService.getUsuarios().subscribe(
      result => {
        this._usuarios = result;
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  OnEliminar(id:number){
    this._usuarioService.deleteUsuario(id).subscribe(
      result =>{ 
        alert('Usuario ha sido Eliminado');
        this.getUsuarios();  
      }, error =>{
        console.log(error);
      }
      );
  }
 
}
 