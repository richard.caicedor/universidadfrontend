import { Component, OnInit } from '@angular/core';
import { Usuario } from '../models/usuario';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  providers: [UsuarioService]
})
export class UsuarioComponent implements OnInit {

	public _usuario:Usuario;
  public _mensaje:string;
  public styleMensaje:string;

	constructor(
    public _usuarioService:UsuarioService
  ) {
    this._usuario = new Usuario(0,'','','','','','','');
   }

	ngOnInit() {
  }
  
  onGuardar(){
    this._usuarioService.addUsuario(this._usuario).subscribe(
      // Funciones de calban 
      response => {
        console.log(response)
        this._mensaje = response.status;
        if(response.code == 401){
          this.styleMensaje = 'alert-danger';  
        }else{
          this.styleMensaje = 'alert-success';
          this._usuario = new Usuario(0,'','','','','','','');
        }
      }, 
      error => {
        console.log(<any>error);
      }
    );
  }

}
 